package romanovilya.mvpatterns.pm.example;

import romanovilya.mvpatterns.common.Example;
import romanovilya.mvpatterns.pm.example.controllers.InputController;
import romanovilya.mvpatterns.pm.example.controllers.LabelController;
import romanovilya.mvpatterns.pm.example.models.Model;
import romanovilya.mvpatterns.pm.example.presentationModels.InputPresentationModel;
import romanovilya.mvpatterns.pm.example.presentationModels.LabelPresentationModel;
import romanovilya.mvpatterns.pm.example.views.InputView;
import romanovilya.mvpatterns.pm.example.views.LabelView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Program {
	public static void main(String[] args) throws IOException {
		Model model = new Model();
		//noinspection unused
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				InputPresentationModel inputPm = new InputPresentationModel(model);
				LabelPresentationModel labelPm = new LabelPresentationModel(model);
				InputController inputController = new InputController(inputPm, br);
				InputView inputView = new InputView(inputPm);
				LabelController labelController = new LabelController(labelPm);
				LabelView labelView = new LabelView(labelPm)) {
			Example.start("PM example", br, o -> inputController.readValue());
		}
	}
}
