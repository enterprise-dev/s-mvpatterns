package romanovilya.mvpatterns.pm.example.views;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.pm.example.presentationModels.LabelPresentationModel;
import romanovilya.mvpatterns.pm.example.presentationModels.PresentationModelChangedEvent;

import java.util.Observable;

public class LabelView extends Observer {
	private LabelPresentationModel pm;

	public LabelView(LabelPresentationModel pm) {
		super((Observable) pm);
		this.pm = pm;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof PresentationModelChangedEvent)) return;
		PresentationModelChangedEvent event = (PresentationModelChangedEvent) arg;

		if (LabelPresentationModel.RESULT_PROPERTY.equals(event.getPropertyName())) {
			System.out.println(event.getPropertyName() + ": " + pm.getResult());
		} else if (LabelPresentationModel.RESULT_COLOR_PROPERTY.equals(event.getPropertyName())) {
			System.out.println(event.getPropertyName() + ": " + pm.getResultColor());
		}
	}
}
