package romanovilya.mvpatterns.pm.example.presentationModels;

import romanovilya.mvpatterns.common.util.Observable;
import romanovilya.mvpatterns.common.util.Observer;

import java.io.Closeable;
import java.io.IOException;

public abstract class BasePresentationModel extends Observable implements java.util.Observer, Closeable {
	private final Observer observer;

	protected BasePresentationModel(java.util.Observable... observables) {
		observer = new Observer(this, observables);
	}

	@Override
	public void update(java.util.Observable o, Object arg) {
		// Default implementation
	}

	@Override
	public void close() throws IOException {
		observer.close();
	}
}
