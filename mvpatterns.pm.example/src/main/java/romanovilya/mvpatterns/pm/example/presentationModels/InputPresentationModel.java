package romanovilya.mvpatterns.pm.example.presentationModels;

import romanovilya.mvpatterns.pm.example.models.Model;
import romanovilya.mvpatterns.pm.example.models.ModelChangedEvent;

import java.util.Observable;

public class InputPresentationModel extends BasePresentationModel {
	public static final String VALUE_PROPERTY = "value";

	private final Model model;

	public InputPresentationModel(Model model) {
		super(model);
		this.model = model;
	}

	public int getValue() {
		return model.getValue();
	}

	public void setValue(int value) {
		model.setValue(value);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof ModelChangedEvent)) return;
		ModelChangedEvent event = (ModelChangedEvent) arg;

		if (Model.VALUE_PROPERTY.equals(event.getPropertyName())) {
			notifyObservers(new PresentationModelChangedEvent(VALUE_PROPERTY));
		}
	}
}
