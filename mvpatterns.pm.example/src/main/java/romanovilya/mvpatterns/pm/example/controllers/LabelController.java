package romanovilya.mvpatterns.pm.example.controllers;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.pm.example.presentationModels.LabelPresentationModel;

public class LabelController extends Observer {
	public LabelController(LabelPresentationModel pm) {
		super((java.util.Observable) pm);
	}
}
