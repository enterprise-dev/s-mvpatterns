package romanovilya.mvpatterns.pm.example.controllers;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.pm.example.presentationModels.InputPresentationModel;

import java.io.BufferedReader;
import java.io.IOException;

public class InputController extends Observer {
	private final InputPresentationModel pm;
	private final BufferedReader br;

	public InputController(InputPresentationModel pm, BufferedReader br) {
		super((java.util.Observable) pm);
		this.pm = pm;
		this.br = br;
	}

	public void readValue() {
		try {
			System.out.print("Enter " + InputPresentationModel.VALUE_PROPERTY + ": ");
			String value = br.readLine();
			pm.setValue(Integer.valueOf(value));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
