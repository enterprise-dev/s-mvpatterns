package romanovilya.mvpatterns.pm.example.views;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.pm.example.presentationModels.InputPresentationModel;
import romanovilya.mvpatterns.pm.example.presentationModels.PresentationModelChangedEvent;

import java.util.Observable;

public class InputView extends Observer {
	private final InputPresentationModel pm;

	public InputView(InputPresentationModel pm) {
		super((Observable) pm);
		this.pm = pm;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof PresentationModelChangedEvent)) return;
		PresentationModelChangedEvent event = (PresentationModelChangedEvent) arg;

		if (InputPresentationModel.VALUE_PROPERTY.equals(event.getPropertyName())) {
			System.out.println(event.getPropertyName() + ": " + pm.getValue());
		}
	}
}
