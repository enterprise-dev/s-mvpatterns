package romanovilya.mvpatterns.pm.example.presentationModels;

public class PresentationModelChangedEvent {
	private final String propertyName;

	public PresentationModelChangedEvent(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
