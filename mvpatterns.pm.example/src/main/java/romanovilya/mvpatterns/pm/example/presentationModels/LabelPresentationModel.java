package romanovilya.mvpatterns.pm.example.presentationModels;

import romanovilya.mvpatterns.pm.example.models.Model;
import romanovilya.mvpatterns.pm.example.models.ModelChangedEvent;

import java.util.Observable;

public class LabelPresentationModel extends BasePresentationModel {
	public static final String RESULT_PROPERTY = "result";
	public static final String RESULT_COLOR_PROPERTY = "resultColor";

	private final Model model;
	private String resultColor;

	public LabelPresentationModel(Model model) {
		super(model);
		this.model = model;
	}

	public int getResult() {
		return model.getResult();
	}

	public String getResultColor() {
		return resultColor;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof ModelChangedEvent)) return;
		ModelChangedEvent event = (ModelChangedEvent) arg;

		if (Model.RESULT_PROPERTY.equals(event.getPropertyName())) {
			notifyObservers(new PresentationModelChangedEvent(RESULT_PROPERTY));
			recalculateResultColor();
		}
	}

	private void recalculateResultColor() {
		resultColor = getResult() >= 0 ? "green" : "red";
		notifyObservers(new PresentationModelChangedEvent(RESULT_COLOR_PROPERTY));
	}
}
