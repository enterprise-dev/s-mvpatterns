package romanovilya.mvpatterns.mvc.example.controllers;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvc.example.models.Model;

import java.io.BufferedReader;
import java.io.IOException;

public class InputController extends Observer {
	private final Model model;
	private final BufferedReader br;

	public InputController(Model model, BufferedReader br) {
		super(model);
		this.model = model;
		this.br = br;
	}

	public void readValue() {
		try {
			System.out.print("Enter " + Model.VALUE_PROPERTY + ": ");
			String value = br.readLine();
			model.setValue(Integer.valueOf(value));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
