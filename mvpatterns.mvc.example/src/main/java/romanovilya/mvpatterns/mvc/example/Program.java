package romanovilya.mvpatterns.mvc.example;

import romanovilya.mvpatterns.common.Example;
import romanovilya.mvpatterns.mvc.example.controllers.InputController;
import romanovilya.mvpatterns.mvc.example.controllers.LabelController;
import romanovilya.mvpatterns.mvc.example.models.Model;
import romanovilya.mvpatterns.mvc.example.views.InputView;
import romanovilya.mvpatterns.mvc.example.views.LabelView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Program {
	public static void main(String[] args) throws IOException {
		Model model = new Model();
		//noinspection unused
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				InputController inputController = new InputController(model, br);
				InputView inputView = new InputView(model);
				LabelController labelController = new LabelController(model);
				LabelView labelView = new LabelView(model)) {
			Example.start("MVC example", br, o -> inputController.readValue());
		}
	}
}
