package romanovilya.mvpatterns.mvc.example.models;

import romanovilya.mvpatterns.common.util.Observable;

import java.util.Random;

public class Model extends Observable {
	public static final String VALUE_PROPERTY = "value";
	public static final String RESULT_PROPERTY = "result";
	public static final String RESULT_COLOR_PROPERTY = "resultColor";

	private final Random random = new Random(System.currentTimeMillis());
	private int value;
	private int result;
	private String resultColor;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
		notifyObservers(new ModelChangedEvent(VALUE_PROPERTY));
		recalculateResult();
	}

	public int getResult() {
		return result;
	}

	public String getResultColor() {
		return resultColor;
	}

	private void recalculateResult() {
		result = value * random.nextInt(13);
		notifyObservers(new ModelChangedEvent(RESULT_PROPERTY));
		recalculateResultColor();
	}

	private void recalculateResultColor() {
		resultColor = result >= 0 ? "green" : "red";
		notifyObservers(new ModelChangedEvent(RESULT_COLOR_PROPERTY));
	}
}
