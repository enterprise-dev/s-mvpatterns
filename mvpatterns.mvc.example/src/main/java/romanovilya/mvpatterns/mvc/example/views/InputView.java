package romanovilya.mvpatterns.mvc.example.views;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvc.example.models.Model;
import romanovilya.mvpatterns.mvc.example.models.ModelChangedEvent;

import java.util.Observable;

public class InputView extends Observer {
	private final Model model;

	public InputView(Model model) {
		super(model);
		this.model = model;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof ModelChangedEvent)) return;
		ModelChangedEvent event = (ModelChangedEvent) arg;

		if (Model.VALUE_PROPERTY.equals(event.getPropertyName())) {
			System.out.println(event.getPropertyName() + ": " + model.getValue());
		}
	}
}
