package romanovilya.mvpatterns.mvc.example.controllers;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvc.example.models.Model;

public class LabelController extends Observer {
	public LabelController(Model model) {
		super(model);
	}
}
