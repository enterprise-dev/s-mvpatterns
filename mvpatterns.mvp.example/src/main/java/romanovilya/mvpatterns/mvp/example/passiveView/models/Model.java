package romanovilya.mvpatterns.mvp.example.passiveView.models;

import romanovilya.mvpatterns.common.util.Observable;

public abstract class Model extends Observable {
	public static final String VALUE_PROPERTY = "value";
	public static final String RESULT_PROPERTY = "result";

	public abstract int getValue();

	public abstract void setValue(int value);

	public abstract int getResult();
}
