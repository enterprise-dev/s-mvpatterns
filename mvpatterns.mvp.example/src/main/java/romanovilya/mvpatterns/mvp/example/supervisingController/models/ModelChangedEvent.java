package romanovilya.mvpatterns.mvp.example.supervisingController.models;

public class ModelChangedEvent {
	private final String propertyName;

	public ModelChangedEvent(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
