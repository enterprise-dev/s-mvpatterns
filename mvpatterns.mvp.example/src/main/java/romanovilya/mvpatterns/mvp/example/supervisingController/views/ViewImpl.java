package romanovilya.mvpatterns.mvp.example.supervisingController.views;

import romanovilya.mvpatterns.mvp.example.supervisingController.models.Model;

import java.io.BufferedReader;
import java.io.IOException;

public class ViewImpl extends View {
	private final BufferedReader br;
	private int value;

	public ViewImpl(Model model, BufferedReader br) {
		super(model);
		this.br = br;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
		System.out.println(VALUE_PROPERTY + ": " + this.value);
	}

	@Override
	public void setResult(int result) {
		System.out.println(RESULT_PROPERTY + ": " + result);
	}

	@Override
	public void setResultColor(String resultColor) {
		System.out.println(RESULT_COLOR_PROPERTY + ": " + resultColor);
	}

	public void readValue() {
		//noinspection Duplicates
		try {
			System.out.print("Enter " + VALUE_PROPERTY + ": ");
			String value = br.readLine();
			this.value = Integer.valueOf(value);
			notifyObservers(new ViewChangedEvent(VALUE_PROPERTY));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
