package romanovilya.mvpatterns.mvp.example.passiveView.views;

public class ViewChangedEvent {
	private final String propertyName;

	public ViewChangedEvent(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
