package romanovilya.mvpatterns.mvp.example;

import romanovilya.mvpatterns.common.Example;
import romanovilya.mvpatterns.mvp.example.supervisingController.models.ModelImpl;
import romanovilya.mvpatterns.mvp.example.supervisingController.presenters.Presenter;
import romanovilya.mvpatterns.mvp.example.supervisingController.views.ViewImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SupervisingControllerProgram extends Program {
	@Override
	public void start() throws IOException {
		ModelImpl model = new ModelImpl();
		//noinspection unused
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				ViewImpl view = new ViewImpl(model, br);
				Presenter presenter = new Presenter(model, view)) {
			Example.start("MVP example (Supervising Controller)", br, o -> view.readValue());
		}
	}
}
