package romanovilya.mvpatterns.mvp.example;

public class Program {
	public void start() throws Exception {
		throw new Exception("Start parameter is not specified or is incorrect");
	}

	public static void main(String[] args) throws Exception {
		Program program = new Program();
		if (args.length > 0) {
			switch (args[0]) {
			case "--passiveView":
				program = new PassiveViewProgram();
				break;
			case "--supervisingController":
				program = new SupervisingControllerProgram();
				break;
			}
		}
		program.start();
	}
}
