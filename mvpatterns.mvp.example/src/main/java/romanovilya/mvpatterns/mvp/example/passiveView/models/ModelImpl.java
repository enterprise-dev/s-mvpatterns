package romanovilya.mvpatterns.mvp.example.passiveView.models;

import java.util.Random;

public class ModelImpl extends Model {
	private final Random random = new Random(System.currentTimeMillis());
	private int value;
	private int result;

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
		notifyObservers(new ModelChangedEvent(VALUE_PROPERTY));
		recalculateResult();
	}

	@Override
	public int getResult() {
		return result;
	}

	private void recalculateResult() {
		result = value * random.nextInt(13);
		notifyObservers(new ModelChangedEvent(RESULT_PROPERTY));
	}
}
