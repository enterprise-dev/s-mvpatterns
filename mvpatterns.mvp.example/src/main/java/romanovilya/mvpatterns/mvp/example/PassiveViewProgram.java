package romanovilya.mvpatterns.mvp.example;

import romanovilya.mvpatterns.common.Example;
import romanovilya.mvpatterns.mvp.example.passiveView.models.ModelImpl;
import romanovilya.mvpatterns.mvp.example.passiveView.presenters.Presenter;
import romanovilya.mvpatterns.mvp.example.passiveView.views.ViewImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PassiveViewProgram extends Program {
	@Override
	public void start() throws IOException {
		ModelImpl model = new ModelImpl();
		ViewImpl view;
		//noinspection unused
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				Presenter presenter = new Presenter(model, view = new ViewImpl(br))) {
			Example.start("MVP example (Passive View)", br, o -> view.readValue());
		}
	}
}
