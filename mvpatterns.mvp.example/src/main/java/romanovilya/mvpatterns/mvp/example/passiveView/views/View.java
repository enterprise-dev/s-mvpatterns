package romanovilya.mvpatterns.mvp.example.passiveView.views;

import romanovilya.mvpatterns.common.util.Observable;

public abstract class View extends Observable {
	public static final String VALUE_PROPERTY = "value";
	public static final String RESULT_PROPERTY = "result";
	public static final String RESULT_COLOR_PROPERTY = "resultColor";

	public abstract int getValue();

	public abstract void setValue(int value);

	public abstract void setResult(int result);

	public abstract void setResultColor(String resultColor);
}
