package romanovilya.mvpatterns.mvp.example.supervisingController.views;

import romanovilya.mvpatterns.common.util.Observable;
import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvp.example.supervisingController.models.Model;
import romanovilya.mvpatterns.mvp.example.supervisingController.models.ModelChangedEvent;

import java.io.Closeable;
import java.io.IOException;

public abstract class View extends Observable implements java.util.Observer, Closeable {
	public static final String VALUE_PROPERTY = "value";
	public static final String RESULT_PROPERTY = "result";
	public static final String RESULT_COLOR_PROPERTY = "resultColor";

	private final Model model;
	private final Observer observer;

	protected View(Model model) {
		this.model = model;
		observer = new Observer((java.util.Observer) this, this.model);
	}

	@Override
	public void update(java.util.Observable o, Object arg) {
		if (arg instanceof ModelChangedEvent) {
			ModelChangedEvent event = (ModelChangedEvent) arg;
			if (event.getPropertyName().equals(Model.VALUE_PROPERTY)) {
				setValue(model.getValue());
			} else if (event.getPropertyName().equals(Model.RESULT_PROPERTY)) {
				setResult(model.getResult());
			}
		}
	}

	@Override
	public void close() throws IOException {
		observer.close();
	}

	public abstract int getValue();

	public abstract void setValue(int value);

	public abstract void setResult(int result);

	public abstract void setResultColor(String resultColor);
}
