package romanovilya.mvpatterns.mvp.example.supervisingController.presenters;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvp.example.supervisingController.models.Model;
import romanovilya.mvpatterns.mvp.example.supervisingController.models.ModelChangedEvent;
import romanovilya.mvpatterns.mvp.example.supervisingController.views.View;
import romanovilya.mvpatterns.mvp.example.supervisingController.views.ViewChangedEvent;

import java.util.Observable;

public class Presenter extends Observer {
	private final Model model;
	private final View view;

	public Presenter(Model model, View view) {
		super(model, view);
		this.model = model;
		this.view = view;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof ModelChangedEvent) {
			ModelChangedEvent event = (ModelChangedEvent) arg;
			if (event.getPropertyName().equals(Model.RESULT_PROPERTY)) {
				view.setResultColor(model.getResult() >= 0 ? "green" : "red");
			}
		} else if (arg instanceof ViewChangedEvent) {
			ViewChangedEvent event = (ViewChangedEvent) arg;
			if (event.getPropertyName().equals(View.VALUE_PROPERTY)) {
				model.setValue(view.getValue());
			}
		}
	}
}
