package romanovilya.mvpatterns.mvvm.example.views;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Binding {
	private final Map<String, List<Consumer>> vmActionMap = new LinkedHashMap<>();
	private final Map<String, List<Consumer>> viewActionMap = new LinkedHashMap<>();

	public Binding oneWay(String key, Consumer action, SideType sideType) {
		Map<String, List<Consumer>> map = SideType.VM_SIDE.equals(sideType) ? vmActionMap : viewActionMap;
		bind(map, key, action);
		return this;
	}

	public Binding oneWay(String key, Consumer vmAction) {
		return oneWay(key, vmAction, SideType.VM_SIDE);
	}

	public Binding twoWay(String key, Consumer vmAction, Consumer viewAction) {
		bind(vmActionMap, key, vmAction);
		bind(viewActionMap, key, viewAction);
		return this;
	}

	public void apply(String key, SideType sideType) {
		Map<String, List<Consumer>> map = SideType.VM_SIDE.equals(sideType) ? vmActionMap : viewActionMap;
		if (map.containsKey(key)) {
			map.get(key).forEach(action -> {
				if (action != null) {
					//noinspection unchecked
					action.accept(null);
				}
			});
		}
	}

	private static void bind(Map<String, List<Consumer>> map, String key, Consumer action) {
		if (!map.containsKey(key)) {
			map.put(key, new LinkedList<>());
		}
		map.get(key).add(action);
	}

	public enum SideType {
		VM_SIDE,
		VIEW_SIDE
	}
}
