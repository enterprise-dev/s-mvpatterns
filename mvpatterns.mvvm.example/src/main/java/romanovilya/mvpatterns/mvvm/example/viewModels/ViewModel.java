package romanovilya.mvpatterns.mvvm.example.viewModels;

import romanovilya.mvpatterns.mvvm.example.models.Model;
import romanovilya.mvpatterns.mvvm.example.models.ModelChangedEvent;

import java.util.Observable;

public class ViewModel extends BaseViewModel {
	public static final String RESULT_COLOR_PROPERTY = "resultColor";

	private final Model model;
	private String resultColor;

	public ViewModel(Model model) {
		super(model);
		this.model = model;
	}

	public Model getModel() {
		return model;
	}

	public String getResultColor() {
		return resultColor;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (!(arg instanceof ModelChangedEvent)) return;
		ModelChangedEvent event = (ModelChangedEvent) arg;

		if (Model.RESULT_PROPERTY.equals(event.getPropertyName())) {
			recalculateResultColor();
		}
	}

	private void recalculateResultColor() {
		resultColor = model.getResult() >= 0 ? "green" : "red";
		notifyObservers(new ViewModelChangedEvent(RESULT_COLOR_PROPERTY));
	}
}
