package romanovilya.mvpatterns.mvvm.example;

import romanovilya.mvpatterns.common.Example;
import romanovilya.mvpatterns.mvvm.example.models.Model;
import romanovilya.mvpatterns.mvvm.example.viewModels.ViewModel;
import romanovilya.mvpatterns.mvvm.example.views.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Program {
	public static void main(String[] args) throws IOException {
		Model model = new Model();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				ViewModel vm = new ViewModel(model);
				View view = new View(vm, br)) {
			Example.start("MVVM example", br, o -> view.readValue());
		}
	}
}
