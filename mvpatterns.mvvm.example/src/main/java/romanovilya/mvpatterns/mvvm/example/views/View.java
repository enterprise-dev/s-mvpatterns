package romanovilya.mvpatterns.mvvm.example.views;

import romanovilya.mvpatterns.common.util.Observer;
import romanovilya.mvpatterns.mvvm.example.models.Model;
import romanovilya.mvpatterns.mvvm.example.models.ModelChangedEvent;
import romanovilya.mvpatterns.mvvm.example.viewModels.ViewModel;
import romanovilya.mvpatterns.mvvm.example.viewModels.ViewModelChangedEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Observable;

import static romanovilya.mvpatterns.mvvm.example.views.Binding.SideType.VIEW_SIDE;
import static romanovilya.mvpatterns.mvvm.example.views.Binding.SideType.VM_SIDE;

public class View extends Observer {
	private final Binding binding = new Binding();

	public View(ViewModel vm, BufferedReader br) {
		super((Observable) vm, vm.getModel());
		binding.twoWay(Model.VALUE_PROPERTY,
				o -> System.out.println(Model.VALUE_PROPERTY + ": " + vm.getModel().getValue()),
				o -> {
					try {
						System.out.print("Enter " + Model.VALUE_PROPERTY + ": ");
						String value = br.readLine();
						vm.getModel().setValue(Integer.valueOf(value));
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				})
				.oneWay(Model.RESULT_PROPERTY,
						o -> System.out.println(Model.RESULT_PROPERTY + ": " + vm.getModel().getResult()))
				.oneWay(ViewModel.RESULT_COLOR_PROPERTY,
						o -> System.out.println(ViewModel.RESULT_COLOR_PROPERTY + ": " + vm.getResultColor()));
	}

	@Override
	public void update(Observable o, Object arg) {
		String key = null;
		if (arg instanceof ModelChangedEvent) {
			key = ((ModelChangedEvent) arg).getPropertyName();
		} else if (arg instanceof ViewModelChangedEvent) {
			key = ((ViewModelChangedEvent) arg).getPropertyName();
		}
		binding.apply(key, VM_SIDE);
	}

	public void readValue() {
		binding.apply(Model.VALUE_PROPERTY, VIEW_SIDE);
	}
}
