package romanovilya.mvpatterns.mvvm.example.viewModels;

import romanovilya.mvpatterns.common.util.Observable;
import romanovilya.mvpatterns.common.util.Observer;

import java.io.Closeable;
import java.io.IOException;

public abstract class BaseViewModel extends Observable implements java.util.Observer, Closeable {
	private final Observer observer;

	protected BaseViewModel(java.util.Observable... observables) {
		observer = new Observer(this, observables);
	}

	@Override
	public void update(java.util.Observable o, Object arg) {
		// Default implementation
	}

	@Override
	public void close() throws IOException {
		observer.close();
	}
}
