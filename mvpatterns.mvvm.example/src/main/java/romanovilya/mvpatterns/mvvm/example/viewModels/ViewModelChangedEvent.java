package romanovilya.mvpatterns.mvvm.example.viewModels;

public class ViewModelChangedEvent {
	private final String propertyName;

	public ViewModelChangedEvent(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
}
