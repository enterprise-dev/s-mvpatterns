package romanovilya.mvpatterns.mvvm.example.models;

import romanovilya.mvpatterns.common.util.Observable;

import java.util.Random;

public class Model extends Observable {
	public static final String VALUE_PROPERTY = "value";
	public static final String RESULT_PROPERTY = "result";

	private final Random random = new Random(System.currentTimeMillis());
	private int value;
	private int result;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
		notifyObservers(new ModelChangedEvent(VALUE_PROPERTY));
		recalculateResult();
	}

	public int getResult() {
		return result;
	}

	private void recalculateResult() {
		result = value * random.nextInt(13);
		notifyObservers(new ModelChangedEvent(RESULT_PROPERTY));
	}
}
