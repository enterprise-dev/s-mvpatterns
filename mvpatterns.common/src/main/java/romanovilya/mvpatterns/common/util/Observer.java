package romanovilya.mvpatterns.common.util;

import java.io.Closeable;
import java.io.IOException;

public class Observer implements java.util.Observer, Closeable {
	private final java.util.Observer observer;
	private final java.util.Observable[] observables;

	public Observer(java.util.Observer observer, java.util.Observable... observables) {
		this.observer = observer != null ? observer : this;
		this.observables = observables;
		for (java.util.Observable observable : this.observables) {
			observable.addObserver(this.observer);
		}
	}

	public Observer(java.util.Observable... observables) {
		this(null, observables);
	}

	@Override
	public void update(java.util.Observable o, Object arg) {
		// Default implementation
	}

	@Override
	public void close() throws IOException {
		for (java.util.Observable observable : observables) {
			observable.deleteObserver(observer);
		}
	}
}
