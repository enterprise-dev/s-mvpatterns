package romanovilya.mvpatterns.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.function.Consumer;

public class Example {
	public static void start(String exampleName, BufferedReader br, Consumer action) throws IOException {
		System.out.println("Welcome to " + exampleName + "...");
		System.out.println();

		while (true) {
			try {
				if (action != null) {
					//noinspection unchecked
					action.accept(null);
				}
			} catch (Throwable e) {
				e.printStackTrace(System.out);
			}

			System.out.println();
			System.out.print("To exit the program, enter \"q\" or \"Q\", to continue enter any key: ");
			String cmd = br.readLine();
			if ("q".equalsIgnoreCase(cmd)) break;
			System.out.println();
		}
	}
}
